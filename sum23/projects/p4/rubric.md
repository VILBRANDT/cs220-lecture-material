# Project 4 (P4) grading rubric 

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don't lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.
- Incorrect function logic loses points in manual code review - for example, incorrectly ordered branches of a conditional statement or  two different incorrectly ordered conditionals (ex: battle function definition)

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)
- `import math` is not written at top of the notebook (-1)
- Used concepts not covered in class yet (-1)
- One or more functions are defined multiple times in the project (-4)

### Question specific guidelines:

- Function 1: damage(attack,defender)
	- `damage` function logic is incorrect (-4)

- Q1 deductions
	- `damage_feraligatr_aipom` answer is hardcoded (-3)
	- `Feraligatr` and `Aipom` are not passed as arguments to `damage` function (-2)

- Q2 deductions
	- `damage_lucario_klawf` answer is hardcoded (-3)
	- `Lucario` and `Klawf` are not passed as arguments to `damage` function (-2)


- Function 2: type_bonus(attack_type, defender)
	- `type_bonus` function logic is incorrect (-4)

- Q3 deductions
	- `bonus_poison_rayquaza` answer is hardcoded (-3)
	- `Poison` and `Rayquaza` are not passed as arguments to `type_bonus` function (-2)

- Q4 deductions
	- `bonus_bug_mew` answer is hardcoded (-3)
	- `Bug` and `Mew` are not passed as arguments to `type_bonus` function (-2)


- Function 3: effective_damage(attacker, defender)
	- `effective_damage`/`get_num_types` function logic is incorrect (-6)

- Q5 deductions
	- `eff_damage_pikachu_weedle` answer is hardcoded (-3)
	- `Pikachu` and `Weedle` are not passed as arguments to `effective_damage` function (-2)

- Q6 deductions
	- `eff_damage_cloyster_lapras` answer is hardcoded (-3)
	- `Cloyster` and `Lapras` are not passed as arguments to `effective_damage` function (-2)

- Q7 deductions
	- `eff_damage_charizard_wailmer` answer is hardcoded (-3)
	- `Charizard` and `Wailmer` are not passed as arguments to `effective_damage` function (-2)


- Function 4: num_hits(attacker, defender)
	- `num_hits` function logic is incorrect (-4)

- Q8 deductions
	- `hits_garchomp_golem` answer is hardcoded (-3)
	- `Golem` and `Garchomp` are not passed as arguments to `num_hits` function (-2)

- Q9 deductions
	- `hits_mew_shinx` answer is hardcoded (-3)
	- `Shinx` and `Mew` are not passed as arguments to `num_hits` function (-2)


- Function 5: battle(pkmn1, pkmn2)
	- `battle` function logic is incorrect (-7)

- Q10 deductions
	- `battle_haunter_rhydon` answer is hardcoded (-4)
	- `Haunter` and `Rhydon` are not passed as arguments to `battle` function (-2)

- Q11 deductions
	- `battle_espeon_raichu` answer is hardcoded (-4)
	- `Espeon` and `Raichu` are not passed as arguments to `battle` function (-2)

- Q12 deductions
	- `battle_garchomp_hydreigon` answer is hardcoded (-4)
	- `Garchomp` and `Hydreigon` are not passed as arguments to `battle` function (-2)

- Q13 deductions
	- `battle_gulpin_mudkip` answer is hardcoded (-4)
	- `Gulpin` and `Mudkip` are not passed as arguments to `battle` function (-2)

- Q14 deductions
	- `battle_miraidon_eevee` answer is hardcoded (-4)
	- `Miraidon` and `Eevee` are not passed as arguments to `battle` function (-2)

- Q15 deductions
	- `battle_onix_hydreigon` answer is hardcoded (-4)
	- `Onix` and `Hydreigon` are not passed as arguments to `battle` function (-2)

- Q16 deductions
	- `battle_chien_pao_tauros` answer is hardcoded (-4)
	- `Chien-Pao` and `Tauros` are not passed as arguments to `battle` function (-2)


- Function 6: friendship_score(pkmn1, pkmn2)
	- `friendship_score` function logic is incorrect (-4)

- Q17 deductions
	- `friendship_aipom_zekrom` answer is hardcoded (-4)
	- `Aipom` and `Zekrom` are not passed as arguments to `friendship_score` function (-2)

- Q18 deductions
	- `friendship_voltorb_thundurus` answer is hardcoded (-4)
	- `Voltorb` and `Thundurus` are not passed as arguments to `friendship_score` function (-2)

- Q19 deductions
	- `friendship_weedle_kakuna` answer is hardcoded (-4)
	- `Weedle` and `Kakuna` are not passed as arguments to `friendship_score` function (-2)

- Q20 deductions
	- `friendship_shinx_shinx` answer is hardcoded (-4)
	- `Shinx` and `Shinx` are not passed as arguments to `friendship_score` function (-2)
